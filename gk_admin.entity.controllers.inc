<?php

/**
 * Entity controller class for bundleable content.
 */
class GKEntityBundleableController extends EntityAPIController {
  public function create(array $values = array()) {
    global $user;

    $values += array(
      'title' => '',
      'status' => 1,
      'uid' => $user->uid,
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );

    return parent::create($values);
  }

  public function save($entity, DatabaseTransaction $transaction = NULL) {
    $entity->changed = REQUEST_TIME;
    return parent::save($entity, $transaction);
  }
}

/**
 * Admin UI controller class for bundleable content.
 */
class GKEntityBundleableUIController extends EntityBundleableUIController {
  public function hook_menu() {
    $items = parent::hook_menu();

    // Redefine the list page since a super class unset it.
    $plural_label = isset($this->entityInfo['plural label']) ? $this->entityInfo['plural label'] : $this->entityInfo['label'] . 's';
    $plural_label = ucfirst($plural_label);

    $items[$this->path] = array(
      'title' => $plural_label,
      'description' => "Manage $plural_label.",
      'page callback' => 'drupal_get_form',
      'page arguments' => array($this->entityType . '_overview_form', $this->entityType),
      'access arguments' => array('administer ' . $this->entityType),
      'file' => 'includes/entity.ui.inc',
    );
    $items[$this->path . '/list'] = array(
      'title' => 'List',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -10,
    );

    // Devel module integration.
    if (module_exists('devel')) {
      $items[$this->path . '/%entity_object/devel'] = array(
        'title' => 'Devel',
        'page callback' => 'devel_load_object',
        'page arguments' => array($this->entityType, $this->id_count),
        'access arguments' => array('access devel information'),
        'type' => MENU_LOCAL_TASK,
        'file' => 'devel.pages.inc',
        'file path' => drupal_get_path('module', 'devel'),
        'weight' => 10,
      );
      $items[$this->path . '/%entity_object/devel/load'] = array(
        'title' => 'Load',
        'type' => MENU_DEFAULT_LOCAL_TASK,
      );

      $items[$this->path . '/%entity_object/devel/render'] = array(
        'title' => 'Render',
        'page callback' => 'devel_render_object',
        'page arguments' => array($this->entityType, $this->id_count),
        'access arguments' => array('access devel information'),
        'file' => 'devel.pages.inc',
        'file path' => drupal_get_path('module', 'devel'),
        'type' => MENU_LOCAL_TASK,
        'weight' => 10,
      );
    }

    return $items;
  }

  public function overviewForm($form, &$form_state) {
    $filters = array();

    if ($filterable_properties = $this->overviewGetFilterableProperties()) {
      $form['filters'] = array(
        '#type' => 'fieldset',
        '#title' => 'Filters',
        '#tree' => TRUE,
        '#collapsible' => TRUE,
      );

      foreach ($filterable_properties as $property) {
        if (isset($_GET[$property]) && $_GET[$property] !== '') {
          $filters[$property] = $_GET[$property];
        }

        switch ($property) {
          case 'title':
            $form['filters']['title'] = array(
              '#type' => 'textfield',
              '#title' => 'Title',
              '#default_value' => isset($filters['title']) ? $filters['title'] : '',
            );
          break;

          case 'type':
            $type_options = array(
              NULL => 'Any',
            );

            foreach ($this->entityInfo['bundles'] as $name => $info) {
              $type_options[$name] = $info['label'];
            }

            $form['filters']['type'] = array(
              '#type' => 'select',
              '#title' => 'Type',
              '#options' => $type_options,
              '#default_value' => isset($filters['type']) ? $filters['type'] : '',
            );
          break;

          case 'status':
            $form['filters']['status'] = array(
              '#type' => 'select',
              '#title' => 'Status',
              '#options' => array(
                NULL => 'Any',
                NODE_PUBLISHED => 'Published',
                NODE_NOT_PUBLISHED => 'Unpublished',
              ),
              '#default_value' => isset($filters['status']) ? $filters['status'] : '',
            );
          break;
        }
      }

      // Close the filters fieldset if we're not filtering.
      $form['filters']['#collapsed'] = empty($filters);

      $form['filters']['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Submit',
        '#weight' => 100,
      );
    }

    $entities = $this->overviewGetEntities($filters);
    $form['table'] = $this->overviewTable($entities);
    $form['pager'] = array('#theme' => 'pager');

    return $form;
  }

  public function overviewFormSubmit($form, &$form_state) {
    form_state_values_clean($form_state);

    $form_state['redirect'] = array($this->path, array(
      'query' => $form_state['values']['filters'],
    ));
  }

  protected function overviewGetEntities($conditions = array()) {
    $entities = array();
    $query = $this->overviewGetQuery($conditions);
    $results = $query->execute();

    if (!empty($results[$this->entityType])) {
      $ids = array_keys($results[$this->entityType]);
      $entities = entity_load($this->entityType, $ids);
    }

    return $entities;
  }

  protected function overviewGetQuery($conditions = array()) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $this->entityType);

    // If the domain module is in use then ensure that only content accessible
    // to the current domain is displayed.
    $query->addTag('domain_access');

    foreach ($conditions as $key => $value) {
      $operator = $key == 'title' ? 'CONTAINS' : '=';
      $query->propertyCondition($key, $value, $operator);
    }

    if ($this->overviewPagerLimit) {
      $query->pager($this->overviewPagerLimit);
    }

    // Order by the primary key column by default.
    $query->propertyOrderBy($this->entityInfo['entity keys']['id']);

    return $query;
  }

  public function overviewTable($entities) {
    $rows = array();

    foreach ($entities as $entity) {
      $id = entity_id($this->entityType, $entity);
      $rows[] = $this->overviewTableRow($id, $entity);
    }

    return array(
      '#theme' => 'table',
      '#header' => $this->overviewTableHeadersSorted(),
      '#rows' => $rows,
      '#empty' => t('None.'),
    );
  }

  protected function overviewTableHeaders() {
    $headers = array(
      'title' => array(
        'label' => 'Title',
        'weight' => 0,
      ),
      'type' => array(
        'label' => 'Type',
        'weight' => 10,
      ),
      'status' => array(
        'label' => 'Status',
        'weight' => 20,
      ),
      'created' => array(
        'label' => 'Created',
        'weight' => 30,
      ),
      'changed' => array(
        'label' => 'Changed',
        'weight' => 40,
      ),
    );

    // If this entity supports operations then add a heading for those too.
    if ($operation_count = $this->operationCount()) {
      $headers['operations'] = array(
        'label' => array(
          'data' => 'Operations',
          'colspan' => $operation_count,
        ),
        'weight' => 50,
      );
    }

    return $headers;
  }

  protected function overviewTableHeadersSorted() {
    // Ensure we only do this once.
    if (isset($this->overview_table_headers_sorted)) {
      return $this->overview_table_headers_sorted;
    }

    $headers = $this->overviewTableHeaders();

    // Sort the headers.
    uasort($headers, function ($a, $b) {
      return $a['weight'] > $b['weight'];
    });

    // Turn the headers into something theme_table() understands.
    $this->overview_table_headers_sorted = array();

    foreach ($headers as $key => $header) {
      $this->overview_table_headers_sorted[$key] = $header['label'];
    }

    return $this->overview_table_headers_sorted;
  }

  protected function overviewTableRow($id, $entity) {
    $row = array();

    foreach ($this->overviewTableHeadersSorted() as $key => $header) {
      // Operations will be handled separately below.
      if ($key == 'operations') {
        continue;
      }

      $row[$key] = $this->overviewTableFieldHandler($key, $entity);
    }

    // Handle the operations column.
    if ($this->operationCount()) {
      $row['edit'] = l('edit', $this->path . '/' . $id . '/edit', array(
        'query' => drupal_get_destination(),
      ));

      $row['delete'] = l('delete', $this->path . '/' . $id . '/delete');
    }

    return $row;
  }

  protected function overviewTableFieldHandler($field, $entity) {
    $value = '';

    switch ($field) {
      case 'title':
        $entity_uri = entity_uri($this->entityType, $entity);

        $value = array('data' => array(
          '#theme' => 'entity_ui_overview_item',
          '#label' => entity_label($this->entityType, $entity),
          '#name' => FALSE,
          '#url' => $entity_uri ? $entity_uri : FALSE,
          '#entity_type' => $this->entityType,
        ));
      break;

      case 'type':
        $value = $this->entityInfo['bundles'][$entity->type]['label'];
      break;

      case 'status':
        if (isset($entity->{$this->statusKey})) {
          $value = $entity->{$this->statusKey} ? 'Published' : 'Unpublished';
        }
      break;

      case 'created':
      case 'changed':
        if (isset($entity->$field)) {
          $value = format_date($entity->$field, 'short');
        }
      break;

      default:
        // Check the entity for a property with the same name as $field.
        if (isset($entity->$field)) {
          $value = $entity->$field;
        }
      break;
    }

    return $value;
  }

  protected function overviewGetFilterableProperties() {
    // Assume that we want to filter by the following properties.
    return array('title', 'type', 'status');
  }

  public function operationFormSubmit($form, &$form_state) {
    parent::operationFormSubmit($form, $form_state);
    $form_state['redirect'] = $this->path;
  }

  protected function operationCount() {
    // We only have edit and delete in our setup.
    return 2;
  }
}
